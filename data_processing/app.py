import boto3
import json
import csv
import io
import os

def lambda_handler(event, context):
    s3_client = boto3.client('s3')
    
    bucket_name = os.getenv('BUCKET_NAME', 'fin-ops-demo')
    file_name = os.getenv('FILE_NAME', 'instance_data_with_metadata.csv')

    try:
        # Download the CSV file from S3
        response = s3_client.get_object(Bucket=bucket_name, Key=file_name)
        content = response['Body'].read().decode('utf-8')
    except Exception as e:
        return {
            "statusCode": 500,
            "body": json.dumps({
                "error": f"Failed to download or read file from S3: {str(e)}"
            })
        }

    # Process the CSV file
    instances_to_downsize = []
    
    try:
        reader = csv.DictReader(io.StringIO(content))
        for row in reader:
            try:
                instance_id = row['InstanceId']
                current_type = row['InstanceType']
                current_cpu_utilization = float(row['CPUUtilization'])
                current_memory_utilization = float(row['MemoryUtilization'])

                # Example logic: downsize if CPU utilization is below 20%
                if current_cpu_utilization < 20.0:
                    instances_to_downsize.append({
                        'InstanceId': instance_id,
                        'InstanceType': current_type,
                        'CPUUtilization': current_cpu_utilization,
                        'MemoryUtilization': current_memory_utilization
                    })
            except KeyError as e:
                print(f"Missing expected column in CSV: {str(e)}")
            except ValueError as e:
                print(f"Invalid data format in CSV: {str(e)}")
            except Exception as e:
                print(f"Unexpected error processing row: {str(e)}")
    except Exception as e:
        return {
            "statusCode": 500,
            "body": json.dumps({
                "error": f"Failed to process CSV file: {str(e)}"
            })
        }

    return {
        "statusCode": 200,
        "body": json.dumps({
            "instances_to_downsize": instances_to_downsize
        })
    }
